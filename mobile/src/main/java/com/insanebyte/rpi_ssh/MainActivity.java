package com.insanebyte.rpi_ssh;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.jcraft.jsch.ChannelExec;



public class MainActivity extends Activity implements SSHClient.ISSHClientListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


    public void connectCommandAction(View view) throws Exception {
//        String host = ((EditText) findViewById(R.id.textEditHost)).getText().toString();
//        String username = ((EditText) findViewById(R.id.textEditUsername)).getText().toString();
//        String password = ((EditText) findViewById(R.id.textEditPassword)).getText().toString();

        // For testing
        String host = "insanebyte.com";
        String username = "dove";
        String password = "L22s@2nN7";

        SSHClient.getInstance().addListener(this);
        SSHClient.getInstance().connect(host, username, password);
    }

    // ISSHClientListener methods impementation
    @Override
    public void connectionAttempted(Boolean connected) {
        String toastMessage = null;
        if (connected) {
            toastMessage = "SSH EXEC channel is successfully opened.";
        } else {
            toastMessage = "SSH EXEC channel was not opened. Please check login credentials network availability.";
        }

        Toast toast = Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_SHORT);
        toast.show();
    }
}
