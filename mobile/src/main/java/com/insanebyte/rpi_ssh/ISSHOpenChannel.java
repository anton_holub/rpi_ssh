package com.insanebyte.rpi_ssh;

import com.jcraft.jsch.ChannelExec;

/**
 * Created by dove on 6/8/15.
 */
public interface ISSHOpenChannel {
    void channelOpened(ChannelExec channelExec);
}
