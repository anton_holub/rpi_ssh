package com.insanebyte.rpi_ssh;

import android.os.AsyncTask;
import android.util.Log;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by dove on 6/17/15.
 */



public class SSHClient implements ISSHOpenChannel{
    // GPIO Console Commands
    private static final String TOGGLE_LIGHT_A_ON = "toggleOn_A";
    private static final String TOGGLE_LIGHT_A_OFF = "toggleOff_A";
    private static final String TOGGLE_LIGHT_B_ON = "toggleOn_B";
    private static final String TOGGLE_LIGHT_B_OFF = "toggleOff_B";
    private static final String TOGGLE_LIGHT_C_ON = "toggleOn_C";
    private static final String TOGGLE_LIGHT_C_OFF = "toggleOff_C";

    // GPIO Lights Identifier
    public enum Light{
        LIGHT_A,
        LIGHT_B,
        LIGHT_C
    }

    private static SSHClient instance;

    private ChannelExec sshExecChannel = null;
    private List<ISSHClientListener> listeners = new ArrayList<ISSHClientListener>();

    private SSHClient () {

    }

    public static synchronized SSHClient getInstance () {
        if (instance == null) {
            instance = new SSHClient();
        }

        return instance;
    }

    // Connection retrieval to remote host via SSH
    public void connect(String host, String username, String password) {
        SshChannelOpenTask sshChannelOpenTask = new SshChannelOpenTask(host, username, password, this);
        sshChannelOpenTask.execute();
    }

    // Executing GPIO commands for making light on and off
    // Should throw actual Exception from JSch, but ok for now to throw common Exception
    public void toggleLight (Light light, Boolean shining) throws Exception {
        String gpioCommand = null;

        switch (light) {
            case LIGHT_A: {
                if (shining) {
                    gpioCommand = TOGGLE_LIGHT_A_ON;
                } else {
                    gpioCommand = TOGGLE_LIGHT_A_OFF;
                }
                break;
            }
            case LIGHT_B: {
                if (shining) {
                    gpioCommand = TOGGLE_LIGHT_B_ON;
                } else {
                    gpioCommand = TOGGLE_LIGHT_B_OFF;
                }
                break;
            }
            case LIGHT_C: {
                if (shining) {
                    gpioCommand = TOGGLE_LIGHT_C_ON;
                } else {
                    gpioCommand = TOGGLE_LIGHT_C_OFF;
                }
                break;
            }

            default: {
                break;
            }
        }

        if ((gpioCommand != null) && (this.sshExecChannel != null)) {
            this.sshExecChannel.setCommand(gpioCommand);
            this.sshExecChannel.connect();
            this.sshExecChannel.disconnect();
        }
    }

    // Managing Listeners list
    public void addListener(ISSHClientListener newListener) {
        if (newListener != null) {
            this.listeners.add(newListener);
        }
    }

    public void removeListener(ISSHClientListener listener) {
        if (listener != null) {
            this.listeners.remove(listener);
        }
    }

    @Override
    public void channelOpened(ChannelExec channelExec) {
        this.sshExecChannel = channelExec;
        Log.i("SshConnection", "Exec channel was retrieved...");

        // Sending notification to listeners
        for (ISSHClientListener listener: this.listeners) {
            listener.connectionAttempted(true);
        }
    }

    // AsyncTask for SSH connection and exec channel retrieving
    private class SshChannelOpenTask extends AsyncTask<Void, Void, ChannelExec> {

        private String hostname = null;
        private String username = null;
        private String password = null;
        private WeakReference<ISSHOpenChannel> weakCallback;

        public SshChannelOpenTask(String hostname, String username, String password, ISSHOpenChannel callback) {
            this.hostname = hostname;
            this.username = username;
            this.password = password;
            this.weakCallback = new WeakReference<ISSHOpenChannel>(callback);
        }

        @Override
        protected ChannelExec doInBackground(Void... params) {
            try {
                JSch jSch = new JSch();
                Session sshSession = jSch.getSession(this.username, this.hostname);
                sshSession.setPassword(this.password);

                // Prevent from asking of SSH key accepting
                Properties prop = new Properties();
                prop.put("StrictHostKeyChecking", "no");
                sshSession.setConfig(prop);
                sshSession.connect();

                return (ChannelExec) sshSession.openChannel("exec");
            } catch (Exception e) {
                // Ugly, but ok for now
                Log.e("SshConnection", e.getLocalizedMessage());

                return null;
            }
        }

        @Override
        protected void onPostExecute(ChannelExec channelExec) {
            if ((channelExec != null) && (this.weakCallback != null)) {
                ISSHOpenChannel callback = weakCallback.get();
                if (callback != null) {
                    callback.channelOpened(channelExec);
                }
            } else {
                Log.w("SshConnection", "No exec channel was retrieved...");
            }
        }
    }

    // Interface declaration for SSHClient listeners
    public interface ISSHClientListener {
        void connectionAttempted(Boolean connected);
    }
}
