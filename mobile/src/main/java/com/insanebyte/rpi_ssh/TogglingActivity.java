package com.insanebyte.rpi_ssh;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;


public class TogglingActivity extends Activity implements CompoundButton.OnCheckedChangeListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toggling);

        // Adding toggling handler for switches
        Switch switchA = ((Switch) findViewById(R.id.switchLightA));
        Switch switchB = ((Switch) findViewById(R.id.switchLightB));
        Switch switchC = ((Switch) findViewById(R.id.switchLightC));
        switchA.setOnCheckedChangeListener(this);
        switchB.setOnCheckedChangeListener(this);
        switchC.setOnCheckedChangeListener(this);
    }

    // Switch toggling
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Switch switchA = ((Switch) findViewById(R.id.switchLightA));
        Switch switchB = ((Switch) findViewById(R.id.switchLightB));
        Switch switchC = ((Switch) findViewById(R.id.switchLightC));

        try {
            if (buttonView == switchA) {
                SSHClient.getInstance().toggleLight(SSHClient.Light.LIGHT_A, switchA.isChecked());
            } else if (buttonView == switchB) {
                SSHClient.getInstance().toggleLight(SSHClient.Light.LIGHT_B, switchB.isChecked());
            } else if (buttonView == switchC) {
                SSHClient.getInstance().toggleLight(SSHClient.Light.LIGHT_C, switchC.isChecked());
            }
        } catch (Exception e) {
            Log.e("TogglingActivity", e.getLocalizedMessage());
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_toggling, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

}
